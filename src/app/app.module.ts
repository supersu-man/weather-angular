import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { TextboxComponent } from './components/textbox/textbox.component';
import { WeathercardComponent } from './components/weathercard/weathercard.component';

@NgModule({
  declarations: [
    AppComponent,
    TextboxComponent,
    WeathercardComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
