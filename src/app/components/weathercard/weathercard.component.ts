import { Component, OnInit, Input} from '@angular/core';

@Component({
  selector: 'app-weathercard',
  templateUrl: './weathercard.component.html',
  styleUrls: ['./weathercard.component.css']
})
export class WeathercardComponent implements OnInit {

  @Input() cityName!: String
  @Input() temperature!: String


  constructor() { }

  ngOnInit(): void {}

}
