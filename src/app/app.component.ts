import { Component, Output, EventEmitter } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { api_key } from '../assets/secrets'

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {
  @Output() fetch = new EventEmitter<String>()
  title = 'weather-angular';

  cityName : String = ""
  temperature : String = ""

  constructor(public http: HttpClient) { }

  fetchData(value: String) {
    requestApi(this, value)
  }
}

async function requestApi(obj : AppComponent, value : String) {
  
  obj.http.get<any>('https://api.openweathermap.org/data/2.5/weather?q=' + value + '&appid=' + api_key, { responseType: 'json' }).subscribe({
    next : (v) => {
      obj.cityName = v['name']
      const celsius = v['main']['temp'] - 273.15
      const rounded = (Math.round( celsius * 10) / 10)
      obj.temperature = rounded +" ° Celsius"
    },
    error : (e) => {
      console.log("Error")
    }
  })

}