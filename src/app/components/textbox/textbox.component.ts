import { Component, OnInit, Output , EventEmitter} from '@angular/core';

@Component({
  selector: 'app-textbox',
  templateUrl: './textbox.component.html',
  styleUrls: ['./textbox.component.css']
})

export class TextboxComponent implements OnInit {

  @Output() btnClick = new EventEmitter<String>()
  
  constructor() {}

  ngOnInit(): void {}

  cityinput: string = ''

  onClick(){
    console.log("Emiting from textbox-component")
    this.btnClick.emit(this.cityinput)
  }

}